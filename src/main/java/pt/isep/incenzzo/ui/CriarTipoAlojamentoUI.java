package pt.isep.incenzzo.ui;

import pt.isep.incenzzo.controller.CriarTipoAlojamentoController;
import pt.isep.incenzzo.domain.Organizacao;
import pt.isep.incenzzo.utils.Utils;

public class CriarTipoAlojamentoUI {

    private Organizacao m_oOrganizacao;
    private CriarTipoAlojamentoController m_controller;

    public CriarTipoAlojamentoUI(Organizacao oOrganizacao)
    {
        this.m_oOrganizacao = oOrganizacao;
        m_controller = new CriarTipoAlojamentoController(m_oOrganizacao);
    }

    public void run()
    {
        System.out.println("\nNovo Tipo de Alojamento:");
        introduzDados();

        apresentaTipoAlojamento();

        if (Utils.confirma("Confirma os dados do Tipo Alojamento? (S/N)")) {
            if (m_controller.guardaTipoAlojamento()) {
                System.out.println("Tipo de Alojamento guardado.");
            } else {
                System.out.println("Tipo de Alojamento não guardado.");
            }
        }
    }

    private void introduzDados() {
        String sDescricao = Utils.readLineFromConsole("Introduza Descricão: ");

        m_controller.criaTipoAlojamento(sDescricao);
    }

    private void apresentaTipoAlojamento()
    {
        System.out.println("\nTipo de Alojamento:\n" + m_controller.getTipoAlojamentoString());
    }
}
