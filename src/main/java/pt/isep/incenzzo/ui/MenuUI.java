package pt.isep.incenzzo.ui;

import java.io.IOException;

import pt.isep.incenzzo.domain.Organizacao;
import pt.isep.incenzzo.utils.Utils;

public class MenuUI {

        private Organizacao m_organizacao;

        public MenuUI(Organizacao organizacao)
        {
            m_organizacao = organizacao;
        }

        public void run() throws IOException
        {
            int opcao = 0;
            do
            {              
                opcao = readOpcaoMenu();

                switch (opcao) {
                    case 1:
                        CriarTipoAlojamentoUI ui1 = new CriarTipoAlojamentoUI(m_organizacao);
                        ui1.run();
                        break;

                    case 7:
                        CriarAlojamentoUI ui7 = new CriarAlojamentoUI(m_organizacao);
                        ui7.run();
                        break;

                }
            }
            while (opcao != 0 );
        }

        private int readOpcaoMenu() {
            System.out.println("\n\n");
            System.out.println("1. Criar Tipo Alojamento\n");
            //System.out.println("2. Criar Tipo Atividade\n");
            //System.out.println("3. Criar Local\n");
            //System.out.println("4. Listar Tipo Alojamento\n");
            //System.out.println("5. Listar Tipo Atividade\n");
            //System.out.println("6. Listar Local\n");
            System.out.println("7. Criar Alojamento\n");
            //System.out.println("8. Criar Atividade\n");
            //System.out.println("9. Criar Pacote\n");

            System.out.println("0. Sair");


            String opcaoStr = Utils.readLineFromConsole("Introduza opção: ");
            
            return Integer.parseInt(opcaoStr);
        }
}
