package pt.isep.incenzzo;

import pt.isep.incenzzo.domain.FactoryAlojamento;
import pt.isep.incenzzo.domain.FactoryTipoAlojamento;
import pt.isep.incenzzo.domain.ImplFactoryAlojamento;
import pt.isep.incenzzo.domain.ImplFactoryTipoAlojamento;
import pt.isep.incenzzo.domain.Organizacao;
import pt.isep.incenzzo.ui.MenuUI;

//Teste
public class Main {

        public static void main(String[] args)
        {
            try
            {
                FactoryTipoAlojamento factoryTipoAlojamento = new ImplFactoryTipoAlojamento();
                FactoryAlojamento factoryAlojamento = new ImplFactoryAlojamento();

                Organizacao organizacao = new Organizacao( factoryTipoAlojamento, factoryAlojamento );

                MenuUI uiMenu = new MenuUI(organizacao);

                uiMenu.run();
            }
            catch( Exception e )
            {
                e.printStackTrace();
            }
        }
}
