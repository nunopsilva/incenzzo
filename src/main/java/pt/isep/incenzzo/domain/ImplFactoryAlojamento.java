package pt.isep.incenzzo.domain;

import pt.isep.incenzzo.domain.Alojamento.DiaSemana;

public class ImplFactoryAlojamento implements FactoryAlojamento {


    public Alojamento criaAlojamento( String sDenominacao, TipoAlojamento oTipoAlojamento, DiaSemana diaSemana,
                                      int nQtdMinPax, int nQtdMaxPax, double fPreco ) {

        return new Alojamento( sDenominacao, oTipoAlojamento, diaSemana, nQtdMinPax, nQtdMaxPax, fPreco );
    }

}
