package pt.isep.incenzzo.domain;

public interface FactoryTipoAlojamento {

    public TipoAlojamento criarTipoAlojamento( String sDenominacao );
    
}
