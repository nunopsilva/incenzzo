package pt.isep.incenzzo.domain;


import java.util.ArrayList;
import java.util.List;

import pt.isep.incenzzo.domain.Alojamento.DiaSemana;

public class Organizacao {

    FactoryTipoAlojamento m_oFactoryTipoAlojamento;
    FactoryAlojamento m_oFactoryAlojamento;

    private final List<TipoAlojamento> m_lstListaTiposAlojamento= new ArrayList<TipoAlojamento>();
    private final List<Alojamento> m_lstListaAlojamento= new ArrayList<Alojamento>();

    public Organizacao( FactoryTipoAlojamento oFactoryTipoAlojamento, FactoryAlojamento oFactoryAlojamento )
    {
        if( oFactoryTipoAlojamento != null )
            this.m_oFactoryTipoAlojamento = oFactoryTipoAlojamento;
        else
            throw new IllegalArgumentException("FactoryTipoAlojamento não pode ser null.");

        if( oFactoryAlojamento != null )
            this.m_oFactoryAlojamento = oFactoryAlojamento;
        else
            throw new IllegalArgumentException("FactoryAlojamento não pode ser null.");
    }

    /*
    // antes de refactory
    public TipoAlojamento novoTipoAlojamento(String desc)
    {
        return (new TipoAlojamento(desc));
    }
    */

    public TipoAlojamento novoTipoAlojamento(String desc)
    {
        return this.m_oFactoryTipoAlojamento.criarTipoAlojamento( desc );
    }

    public boolean validaTipoAlojamento(TipoAlojamento ta)
    {
        if( ta != null )
            return true;
        else
            return false;
    }

    public boolean guardaTipoAlojamento(TipoAlojamento ta)
    {
        if (this.validaTipoAlojamento(ta))
        {
            return m_lstListaTiposAlojamento.add(ta);
        }
        return false;
    }

    /*
    public Alojamento novoAlojamento(String desc, TipoAlojamento oTipoAlojamento, DiaSemana diaSemana, int nQtdMinPax, int nQtdMaxPax, double fPreco )
    {
        return new Alojamento(desc, oTipoAlojamento, diaSemana, nQtdMinPax, nQtdMaxPax, fPreco);
    }
    */

    public Alojamento novoAlojamento(String desc, TipoAlojamento oTipoAlojamento, DiaSemana diaSemana, int nQtdMinPax, int nQtdMaxPax, double fPreco )
    {
        return this.m_oFactoryAlojamento.criaAlojamento(desc, oTipoAlojamento, diaSemana, nQtdMinPax, nQtdMaxPax, fPreco);
    }

    public boolean validaAlojamento(Alojamento oAlojamento )
    {
        if( oAlojamento != null )
            return true;
        else
            return false;
    }

    public boolean guardaAlojamento(Alojamento oAlojamento)
    {
        if (this.validaAlojamento(oAlojamento))
        {
            return m_lstListaAlojamento.add(oAlojamento);
        }
        return false;
    }

    public List<TipoAlojamento> getTipoAlojamentos() {

        List<TipoAlojamento> lstTipoAlojamentos = new ArrayList<TipoAlojamento>();

        lstTipoAlojamentos.addAll( this.m_lstListaTiposAlojamento );

        return lstTipoAlojamentos;
    }
}
