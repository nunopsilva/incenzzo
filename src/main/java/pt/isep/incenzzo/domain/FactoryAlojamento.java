package pt.isep.incenzzo.domain;

import pt.isep.incenzzo.domain.Alojamento.DiaSemana;

public interface FactoryAlojamento {

    public Alojamento criaAlojamento( String sDenominacao, TipoAlojamento oTipoAlojamento, DiaSemana diaSemana,
                                      int nQtdMinPax, int nQtdMaxPax, double fPreco );
}
