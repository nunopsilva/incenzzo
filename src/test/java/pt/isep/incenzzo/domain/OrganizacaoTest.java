package pt.isep.incenzzo.domain;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import pt.isep.incenzzo.domain.FactoryAlojamento;
import pt.isep.incenzzo.domain.FactoryTipoAlojamento;
import pt.isep.incenzzo.domain.Organizacao;
import pt.isep.incenzzo.domain.TipoAlojamento;

/**
 * Unit test for Organizacao.
 */
public class OrganizacaoTest {

    @Test
    public void shouldCreateAValidOrganizacao() throws Exception
    {
        FactoryTipoAlojamento factoryTipoAlojamentoDouble = mock( FactoryTipoAlojamento.class);
        FactoryAlojamento factoryAlojamentoDouble = mock( FactoryAlojamento.class);

        new Organizacao( factoryTipoAlojamentoDouble, factoryAlojamentoDouble );
    }


    @Test
    public void shouldThrowExceptionWithEmptyFactoryTipoAlojamento_inJUnit5() throws Exception
    {
        Exception exception = assertThrows(Exception.class, () -> {
            new Organizacao(null, null);
        });
    
        String expectedMessage = "FactoryTipoAlojamento não pode ser null.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void shouldThrowExceptionWithEmptyFactoryAlojamento_inJUnit5() throws Exception
    {
        FactoryTipoAlojamento factoryTipoAlojamentoDouble = mock( FactoryTipoAlojamento.class);

        Exception exception = assertThrows(Exception.class, () -> {
            new Organizacao(factoryTipoAlojamentoDouble, null);
        });
    
        String expectedMessage = "FactoryAlojamento não pode ser null.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    public void shouldReturnANewTipoAlojamento()
    {
        // Arrange
        FactoryTipoAlojamento factoryTipoAlojamentoDouble = mock( FactoryTipoAlojamento.class);
        FactoryAlojamento factoryAlojamentoDouble = mock( FactoryAlojamento.class);

        Organizacao organizacao = new Organizacao( factoryTipoAlojamentoDouble, factoryAlojamentoDouble );

        TipoAlojamento oTipoAlojamentoDouble = mock( TipoAlojamento.class );
        when( factoryTipoAlojamentoDouble.criarTipoAlojamento( "desc" )).thenReturn( oTipoAlojamentoDouble );

        // Act
        TipoAlojamento oTipoAlojamento = organizacao.novoTipoAlojamento("desc");

        // Assert
        assertEquals( oTipoAlojamento, oTipoAlojamentoDouble );
    }

}
